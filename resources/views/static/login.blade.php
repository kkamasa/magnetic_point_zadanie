@extends('static.layout')

@section('content')

    <div class="container">
        {!! Form::open(['action' => 'AuthController@postLogin', 'class' =>'form-signin', 'data-toggle'=>'validator', 'role'=>'form','id'=> 'form']) !!}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h2 class="form-signin-heading">{{ trans('web.login_title') }}</h2>
        <hr>

        <label for="email" class="sr-only">{{ trans('web.input_email') }}</label>
        {!! Form::email('email', null, ['class'=> 'form-control', 'placeholder'=> trans('web.input_email'), 'autofocus' => true])!!}
        <label for="password" class="sr-only">{{trans('web.input_password')}}</label>
        {!! Form::password('password', ['class'=> 'form-control', 'placeholder'=> trans('web.input_password')])!!}
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('web.login_btn') }}</button>
        {!! Form::close() !!}
    </div>

@stop

@section('scripts_bottom')
    <script src="{{ asset('components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script>
        $('#form').validate({
            cancelSubmit: true,
            errorClass: "text-danger",
            validClass: "success",
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: "{{ trans('web.err_msg_email_required') }}",
                    email: "{{ trans('web.err_msg_email_email') }}"
                },
                password: {
                    required: "{{ trans('web.err_msg_password_required') }}"
                }
            }
        })
    </script>
@stop