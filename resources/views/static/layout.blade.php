<!DOCTYPE html>
<html>
<head>
    <title>Magnetic point zadanie</title>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/flag-icon-css/1.0.0/css/flag-icon.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css">

</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('') }}">Magnetic point zadanie</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="{{ action('StaticController@getIndex') }}">{{ trans('web.menu_home') }}</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="label"><a class="flag-icon flag-icon-pl" href="{{ action('LanguageController@getChange', ['locale' => 'pl']) }}"></a></li>
                <li class="label"><a class="flag-icon flag-icon-gb" href="{{ action('LanguageController@getChange', ['locale' => 'en']) }}"></a></li>
                @if(empty(Auth::user()))
                    <li><a href="{{ action('StaticController@getLogin') }}">{{ trans('web.menu_login') }}</a></li>
                    <li><a href="{{ action('StaticController@getRegister') }}">{{ trans('web.menu_register') }}</a></li>
                @else
                    <li><a>{{Auth::user()->name}}, {{Auth::user()->lastName}}</a></li>
                    <li><a href="{{ action('AuthController@getLogout') }}">{{ trans('web.menu_logout') }}</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="container">
        @if (!empty(Session::get('dangerAlert')))
            <div class="alert alert-danger">
                <p>{{Session::get('dangerAlert')}}</p>
            </div>
        @endif

        @if (!empty(Session::get('successAlert')))
            <div class="alert alert-success">
                <p>{{Session::get('successAlert')}}</p>
            </div>
        @endif

    @yield('content')
</div>

<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

@yield('scripts_bottom')

</body>
</html>
