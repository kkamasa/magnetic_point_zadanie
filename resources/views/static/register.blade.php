@extends('static.layout')

@section('content')

    <div class="container">
        {!! Form::open(['action' => 'AuthController@postRegister', 'class' =>'form-signin form-horizontal', 'data-toggle'=>'validator', 'role'=>'form','id'=> 'form']) !!}
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h2 class="form-signin-heading ">{{ trans('web.register_title') }}</h2>
        <hr>
        <label for="email">{{ trans('web.input_email') }} *</label>
        {!! Form::email('email', null, ['class'=> 'form-control', 'placeholder'=>trans('web.input_email'), 'autofocus' => true])!!}
        <label for="password">{{ trans('web.input_password') }} *</label>
        {!! Form::password('password', ['class'=> 'form-control', 'placeholder'=> trans('web.input_password')])!!}
        <label for="password_confirmation">{{ trans('web.input_password_confirmation') }} *</label>
        {!! Form::password('password_confirmation', ['class'=> 'form-control', 'placeholder'=>trans('web.input_password_confirmation')])!!}
        <hr>
        <label>{{ trans('web.input_birthdate') }} *</label>
        <input name="birthdate" type="hidden">
        <div class="form-group">
            <div class="col-xs-4">
                {!! Form::text('day', null, ['class'=> 'form-control', 'placeholder'=> trans('web.input_day')])!!}
            </div>
            <div class="col-xs-4">
                {!! Form::select('month', [ 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj',
                    'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik',
                    'Listopad', 'Grudzień' ], null, [ 'class'=> 'form-control', 'placeholder' => trans('web.input_month')]) !!}
            </div>
            <div class="col-xs-4">
                {!! Form::text('year', null, ['class'=> 'form-control', 'placeholder'=>trans('web.input_year')])!!}
            </div>
        </div>
        <label for="name">{{ trans('web.input_name') }}</label>
        {!! Form::text('name', null, ['class'=> 'form-control', 'placeholder'=> trans('web.input_name')])!!}
        <label for="lastName">{{ trans('web.input_lastname') }}</label>
        {!! Form::text('lastName', null, ['class'=> 'form-control', 'placeholder'=> trans('web.input_lastname')])!!}
        <hr>
        <button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('web.register_btn') }}</button>
        {!! Form::close() !!}
    </div>

@stop

@section('scripts_bottom')
    <script src="{{ asset('components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('components/moment/moment.js') }}"></script>
    <script>
        $(function () {
            var updateBirthdate = function () {
                var year = $('input[name=year]').val();
                var month = $('select[name=month]').val();
                var day = $('input[name=day]').val();
                $('input[name=birthdate ]').val(moment([
                        year, month++, day
                ]).format('YYYY-MM-DD'));
                return true;
            };

            $.validator.addMethod("day", function(value) {
                var year = $('input[name=year]').val();
                var month = $('select[name=month]').val();
                if(!year || !month)
                    return true;
                return moment([year, month++, value]).isValid();
            });

            $('#form').validate({
                cancelSubmit: true,
                errorClass: "text-danger",
                validClass: "success",
                errorElement: "p",
                submitHandler: updateBirthdate,
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: '{{action('AuthController@getCheckEmail')}}',
                            method: 'get',
                        }
                    },
                    password: {
                        required: true,
                        minlength: 8
                    },
                    password_confirmation: {
                        equalTo: "input[name=password]"
                    },
                    day: {
                        required: true,
                        number: true,
                        day: true
                    },
                    month: {
                        required: true,
                        number: true,
                        max: 12
                    },
                    year: {
                        required: true,
                        number: true,
                        max: moment().year(),
                        min: 1900
                    }
                },
                messages: {
                    email: {
                        required: "{{ trans('web.err_msg_email_required') }}",
                        email: "{{ trans('web.err_msg_email_email') }}",
                        remote: "{{ trans('web.err_msg_email_remote') }}"
                    },
                    password: {
                        required: "{{ trans('web.err_msg_password_required') }}",
                        minlength: $.validator.format("{{ trans('web.err_msg_password_minlength') }}")
                    },
                    password_confirmation: {
                        equalTo: "{{ trans('web.err_msg_password_confirmation_equalTo') }}"
                    },
                    day: {
                        required: "{{ trans('web.err_msg_any_required') }}",
                        day: "{{ trans('web.err_msg_day_day') }}",
                        number: "{{ trans('web.err_msg_any_number') }}"
                    },
                    month: {
                        required: "{{ trans('web.err_msg_any_required') }}"
                    },
                    year:{
                        required: "{{ trans('web.err_msg_any_required') }}",
                        number: "{{ trans('web.err_msg_any_number') }}",
                        max: $.validator.format("{{ trans('web.err_msg_year_max') }}"),
                        min: $.validator.format("{{ trans('web.err_msg_year_min') }}")
                    }
                }
            })
        })
    </script>
@stop