@extends('static.layout')

@section('content')

    <div class="jumbotron">
        <div class="container">
            @if(!Auth::check())
                <h1>{{ trans('web.greetings_not_logged') }}</h1>
            @else
                <h1>{{ trans('web.greetings_info', ['username' => Auth::user()->name]) }}</h1>
            @endif
        </div>
    </div>

@stop