<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2> Witaj,</h2>

<div>
    Aby potwierdzić rejestrację w serwisie, proszę kliknąć w poniższy link:<br>
    {{ action('AuthController@getVerify',['confirmationCode' => $confirmationCode]) }}<br>
    Jeśli nie możesz kliknąć w link, skopiuj go w całości i wklej do paska adresu w przeglądarce<br/>
</div>

</body>
</html>