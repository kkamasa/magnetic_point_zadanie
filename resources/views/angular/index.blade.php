<!DOCTYPE html>
<html>
<head>
    <title>Magnetic point zadanie-angular</title>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/angular-ui-notification/dist/angular-ui-notification.min.css') }}" rel="stylesheet" type="text/css">
    <link rel='stylesheet' href='{{ asset('components/angular-loading-bar/build/loading-bar.min.css') }}' type='text/css' media='all' />
</head>
<body ng-app="magneticAngularApp">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#">Magnetic point zadanie-angular</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li ng-hide="$root.Auth.is()"><a href="#/login">Logowanie</a></li>
                <li ng-hide="$root.Auth.is()"><a href="#/register">Rejestracja</a></li>
                <li ng-show="$root.Auth.is()"><a ng-bind="$root.Auth.user().name +' '+$root.Auth.user().lastName"></a></li>
                <li ng-show="$root.Auth.is()"><a ng-click="$root.Auth.logout()">Wyloguj</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" ng-view="">

</div>

{{--helpers--}}
<script>
    var view = function (name) {
        return "{{ asset('app/views') }}/"+name+'.html';
    }
</script>

<script src="{{ asset('components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('components/moment/moment.js') }}"></script>
<script src="{{ asset('components/angular/angular.min.js') }}"></script>
<script src="{{ asset('components/angular-route/angular-route.min.js') }}"></script>
<script src="{{ asset('components/a0-angular-storage/dist/angular-storage.min.js') }}"></script>
<script src="{{ asset('components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('components/jpkleemans-angular-validate/dist/angular-validate.min.js') }}"></script>
<script src="{{ asset('components/angular-ui-notification/dist/angular-ui-notification.min.js') }}"></script>
<script src="{{ asset('components/angular-animate/angular-animate.min.js') }}"></script>
<script src="{{ asset('components/angular-loading-bar/build/loading-bar.min.js') }}"></script>

<script src="{{ asset('app/app.js') }}"></script>
<script src="{{ asset('app/services/auth.js') }}"></script>
<script src="{{ asset('app/services/alert.js') }}"></script>
<script src="{{ asset('app/controllers/main.js') }}"></script>
<script src="{{ asset('app/controllers/about.js') }}"></script>

</body>
</html>
