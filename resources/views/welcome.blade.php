<!DOCTYPE html>
<html>
<head>
    <title>Magnetic point zadanie</title>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('components/bootstrap/dist/css/bootstrap-theme.min.css') }}" rel="stylesheet" type="text/css">

</head>
<body>
<div class="container">
    <div class="content">
        <div class="page-header">
            <h1>Magnetic point <small>zadanie domowe</small></h1>
        </div>
        <div class="jumbotron">
            <div class="container">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="{{ action('StaticController@getIndex') }}" class="btn btn-primary btn-lg">Static Html <i class="fa fa-html5"></i></a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="{{ url('angular/') }}" type="button" class="btn btn-info btn-lg">Angular <i class="glyphicon glyphicon-gift"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
