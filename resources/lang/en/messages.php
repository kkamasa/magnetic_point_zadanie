<?php

return [
    'failed_login_attempt' => 'E-mail address or password you entered is incorrect.',
    'inactive_account' => 'The account is not activated.',
    'account_created_successfully' => 'The account has been created and is waiting for an e-mail activation.',
    'failed_create_account' => 'The account has not been created, please contact administrator.',
    'account_activated_successfully' => 'The account has been activated.',
    'failed_find_user_by_confirmation_code' => 'The code is not active.'
];
