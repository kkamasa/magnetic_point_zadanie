<?php

return [
    'greetings_info' => 'Hello, :username.  ',
    'greetings_not_logged' => 'You are not logged in.',

    'menu_login' => 'Sign in',
    'menu_register' => 'Sign up',
    'menu_logout' => 'Log out',
    'menu_home' => 'Home',

    'login_title' => 'Sign in',
    'login_btn' => 'Sign in',
    'register_title' => 'Sign up',
    'register_btn' => 'Create an account',

    'err_msg_email_required' => 'Provide an e-mail address is required.',
    'err_msg_email_email' => 'The e-mail address is valid.',
    'err_msg_email_remote' => 'The e-mail address is already in use',
    'err_msg_password_required' => 'Provide a password is required.',
    'err_msg_password_minlength' => 'The password must include {0} characters.',
    'err_msg_password_confirmation_equalTo' => 'These passwords don\'t match.',
    'err_msg_any_required' => 'You can\'t leave this empty.',
    'err_msg_any_number' => 'Please enter a number.',
    'err_msg_day_day' => 'Check date, it doesn\'t look properly.',
    'err_msg_year_max' => 'Year of birth cannot be bigger than {0}.',
    'err_msg_year_min' => 'Year of birth cannot be smaller than {0}.',

    'input_email' => 'Email',
    'input_password' => 'Password',
    'input_password_confirmation' => 'Confirm your password',
    'input_birthdate' => 'Birth date',
    'input_day' => 'Day',
    'input_month' => 'Month',
    'input_year' => 'Year',
    'input_name' => 'First Name',
    'input_lastname' => 'Last Name',
];
