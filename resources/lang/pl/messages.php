<?php

return [
    'failed_login_attempt' => 'Wprowadzony adres e-mail lub hasło są nieprawidłowe.',
    'inactive_account' => 'Konto nie zostało aktywowane.',
    'account_created_successfully' => 'Konto zostało utworze i czeka na aktywację email.',
    'failed_create_account' => 'Konto nie zostało utworze, prosze skontaktować się z administratorem serwisu.',
    'account_activated_successfully' => 'Konto zostało zaaktywowane.',
    'failed_find_user_by_confirmation_code' => 'Kod nie jest aktywny.'
];
