<?php

return [
    'greetings_info' => 'Witaj, :username.  ',
    'greetings_not_logged' => 'Nie jesteś zalogowany.',

    'menu_login' => 'Logowanie',
    'menu_register' => 'Rejestracja',
    'menu_logout' => 'Wyloguj',
    'menu_home' => 'Home',

    'login_title' => 'Logowanie',
    'login_btn' => 'Zaloguj',
    'register_title' => 'Rejestracja',
    'register_btn' => 'Utwórz konto',

    'err_msg_email_required' => 'Podanie adresu email jest wymagane.',
    'err_msg_email_email' => 'Podany adres email ma zły format.',
    'err_msg_email_remote' => 'Podany adres jest już zarejestrowany',
    'err_msg_password_required' => 'Podanie hasła jest wymagane.',
    'err_msg_password_minlength' => 'Hasło musi się składać z minimum {0} znaków.',
    'err_msg_password_confirmation_equalTo' => 'Hasło nie jest identyczne.',
    'err_msg_any_required' => 'Pole jest wymagane.',
    'err_msg_any_number' => 'Proszę podać liczbę.',
    'err_msg_day_day' => 'Sprawdz date, nie wyglada poprawnie.',
    'err_msg_year_max' => 'Rok urodzenia nie może byc wiekszy od {0}.',
    'err_msg_year_min' => 'Rok urodzenia nie może byc mniejszy od {0}.',

    'input_email' => 'Email',
    'input_password' => 'Hasło',
    'input_password_confirmation' => 'Powtórz Hasło',
    'input_birthdate' => 'Data Urodzenia',
    'input_day' => 'Dzień',
    'input_month' => 'Miesiąc',
    'input_year' => 'Rok',
    'input_name' => 'Imię',
    'input_lastname' => 'Nazwisko',
];
