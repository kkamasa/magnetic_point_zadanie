<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function postLogin(Request $request)
    {
        $user = User::where(['email' => $request->get('email')])->first();

        // check user and password
        if(!$user || !Hash::check($request->get('password'), $user->password)){
            if($request->ajax()){
                return response([
                    'dangerAlert' => trans('messages.failed_login_attempt')
                ], 400);
            }else{
                return redirect()
                    ->back()
                    ->with('dangerAlert', trans('messages.failed_login_attempt'));
            }
        }

        // check is active
        if(!$user->active){
            if($request->ajax()){
                return response([
                    'dangerAlert' => trans('messages.inactive_account')
                ], 400);
            }else{
                return redirect()
                    ->action('StaticController@getIndex')
                    ->with('dangerAlert', trans('messages.inactive_account'));
            }
        }

        // login
        Auth::login($user);
        if($request->ajax()){
            return response($user, 200);
        }else{
            return redirect()
                ->action('StaticController@getIndex');
        }
    }

    public function postRegister(Requests\RegisterRequest $request)
    {
        $confirmationCode = str_random(env('CONFIRMATION_CODE'));

        // create new user
        if(User::create(array_merge(
            $request->only([
                'email', 'name', 'lastName', 'birthdate'
            ]),[
                'password' => Hash::make($request->get('password')),
                'confirmationCode' => $confirmationCode
            ]
        ))){
            // send mail
            Mail::send('email.verify', ['confirmationCode' => $confirmationCode], function ($m) use ($request) {
                $m->from(env('MAIL_USERNAME'), trans('email.from_name'));
                $m->to($request->get('email'), $request->get('name'))->subject(trans('email.subject'));
            });
            if($request->ajax()) {
                return response([
                    'successAlert'=> trans('messages.account_created_successfully')
                ], 200);
            }else{
                return redirect()
                    ->action('StaticController@getIndex')
                    ->with('successAlert', trans('messages.account_created_successfully'));
            }

        }else{
            // if fail back to index
            return redirect()
                ->action('StaticController@getIndex')
                ->with('dangerAlert', trans('messages.failed_create_account'));
        }
    }

    public function getCheckEmail(Request $request)
    {
        return !User::where('email', $request->get('email'))->first()? 'true' : 'false';
    }

    public function getVerify($confirmationCode)
    {
        $user = User::where(['confirmationCode' => $confirmationCode])->first();
        if(!$user){
            return redirect()
                ->action('StaticController@getIndex')
                ->with('dangerAlert', trans('messages.failed_find_user_by_confirmation_code'));
        }

        if($user->update([
            'active' => true,
            'confirmationCode' => null
        ])){
            return redirect()
                ->action('StaticController@getLogin')
                ->with('successAlert', trans('messages.account_activated_successfully'));
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()
            ->action('StaticController@getIndex');
    }
}
