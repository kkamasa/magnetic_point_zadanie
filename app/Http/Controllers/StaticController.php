<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaticController extends Controller
{
    public function getIndex()
    {
        return view('static.index');
    }

    public function getLogin()
    {
        return view('static.login');
    }

    public function getRegister()
    {
        return view('static.register');
    }
}
