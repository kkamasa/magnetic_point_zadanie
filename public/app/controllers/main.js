'use strict';

/**
 * @ngdoc function
 * @name magneticAngularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the magneticAngularApp
 */
angular.module('magneticAngularApp')
    .controller('MainCtrl', function ($scope, Auth,  Alert, $location) {

        //Login
        $scope.loginSubmit = function(){
            $scope.lSpinner = true;
            Auth.login($scope.lData, function (err, res) {
                Alert(res);
                Alert(err);
                if(res){
                    $location.path('/')
                }
                $scope.lSpinner = false;
            })
        };

        $scope.lData = {};

        //Register
        $scope.registerSubmit = function(form){
            if(form.validate()) {
                $scope.rData.birthdate = moment([
                    $scope.year, $scope.month++, $scope.day
                ]).format('YYYY-MM-DD');
                $scope.rSpinner = true;
                Auth.register($scope.rData, function (err, res) {
                    if(err){
                        $scope.backendErrors = err;
                    }else{
                        Alert(res);
                        $location.path('/')
                    }
                    $scope.rSpinner = false;
                })
            }
        };

        $scope.registerValidationOptions = {
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '/auth/check-email',
                        method: 'get'
                    }
                },
                password: {
                    required: true,
                    minlength: 8
                },
                password_confirmation: {
                    equalTo: "input[name=password]"
                },
                day: {
                    required: true,
                    number: true,
                    day: true
                },
                month: {
                    required: true,
                    number: true,
                    max: 12
                },
                year: {
                    required: true,
                    number: true,
                    max: moment().year(),
                    min: 1900
                }
            },
            messages: {
                email: {
                    required: "Podanie adresu email jest wymagane",
                    email: "Podany adres email ma zły format",
                    remote: "Podany adres jest już zarejestrowany"
                },
                password: {
                    required: "Podanie hasła jest wymagane",
                    minlength: $.validator.format("Hasło musi się składać z minimum {0} znaków")
                },
                password_confirmation: {
                    equalTo: "Hasło nie jest identyczne"
                },
                day: {
                    required: "Pole jest wymagane",
                    day: "Sprawdz date, nie wyglada poprawnie",
                    number: "Proszę podać liczbę"
                },
                month: {
                    required: "Pole jest wymagane"
                },
                year: {
                    required: "Pole jest wymagane",
                    number: "Proszę podać liczbę",
                    max: $.validator.format("Rok urodzenia nie może byc wiekszy od {0}"),
                    min: $.validator.format("Rok urodzenia nie może byc mniejszy od {0}")
                }
            }
        };

        $scope.rData = {};

        $scope.moths = [ 'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj',
            'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik',
            'Listopad', 'Grudzień' ];
    });
