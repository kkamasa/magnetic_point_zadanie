'use strict';

/**
 * @ngdoc function
 * @name magneticAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the magneticAngularApp
 */
angular.module('magneticAngularApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
