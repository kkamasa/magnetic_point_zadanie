'use strict';

angular.module('magneticAngularApp').service('Alert', function (Notification) {
   return function (data) {
       if(!data) { return }
       if(data.successAlert){
           Notification.success(data.successAlert);
       }
       if(data.dangerAlert){
           Notification.error(data.dangerAlert);
       }
   }
});