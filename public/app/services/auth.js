'use strict';

angular.module('magneticAngularApp').service('Auth', function (store, $http) {
    var that = this;

    this.is = function () {
        return store.get('user')? true : false;
    };

    this.user = function () {
        return store.get('user');
    };
    
    this.register = function (data, cb) {
        $http.post('/auth/register', data)
            .success(function (res) {
               cb(null, res);
            })
            .error(cb)
    };

    this.login = function (data, cb) {
        $http.post('/auth/login', data)
            .success(function (res) {
                store.set('user', res);
                cb(null, res);
            })
            .error(function (err) {
                cb(err)
            })
    }

    this.logout = function (cb) {
        $http.get('/auth/logout')
            .success(function () {
                store.remove('user');
                if(cb){
                    cb(null);
                }
            })
            .error(function (err) {
                console.error(err)
            })
    }

});