'use strict';

/**
 * @ngdoc overview
 * @name magneticAngularApp
 * @description
 * # magneticAngularApp
 *
 * Main module of the application.
 */
angular
    .module('magneticAngularApp', [
        'ngRoute',
        'ngValidate',
        'ui-notification',
        'angular-storage',
        'angular-loading-bar',
        'ngAnimate'
    ])

    .run(function (Auth, $rootScope) {
        $rootScope.Auth = Auth;
    })

    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }])

    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 15000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
    })

    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }])

    .config(function ($validatorProvider) {
        $validatorProvider.setDefaults({
            errorElement: 'p',
            errorClass: 'text-danger'
        });
    })

    .config(function ($validatorProvider) {
        $validatorProvider.addMethod("day", function (value) {
            var year = $('input[name=year]').val();
            var month = $('select[name=month]').val();
            if(!year || !month)
                return true;
            return moment([year, month++, value]).isValid();
        })
    })

    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: view('main'),
                controller: 'MainCtrl'
            })
            .when('/register', {
                templateUrl: view('register'),
                controller: 'MainCtrl'
            })
            .when('/login', {
                templateUrl: view('login'),
                controller: 'MainCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
